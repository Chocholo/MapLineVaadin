package com.chocholo;

import com.chocholo.GoogleApi.GooglePlaceApiRest;
import com.chocholo.Util.HibernateUtil;
import com.chocholo.Util.WindowUtil;
import com.chocholo.VaadinComponents.Place.PlaceAddModifyWindow;
import com.chocholo.VaadinComponents.Place.PlaceGrid;
import com.chocholo.VaadinComponents.Place.PlaceManipulation;
import com.chocholo.VaadinComponents.Trip.TripAddModifyWindow;
import com.chocholo.VaadinComponents.Trip.TripGrid;
import com.chocholo.VaadinComponents.Trip.TripManipulation;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.hibernate.Session;

/**
 * Created by Chocholo on 15.02.2017.
 */
public class MapLineMain extends UI {

    VerticalLayout verticalTrip = new VerticalLayout();
    VerticalLayout verticalPlace = new VerticalLayout();
    HorizontalLayout horizontalLayout = new HorizontalLayout();
    TripGrid tripGrid = new TripGrid();
    PlaceGrid placeGrid = new PlaceGrid();
    PlaceAddModifyWindow placeAddModifyWindow = new PlaceAddModifyWindow(placeGrid);
    TripAddModifyWindow tripAddModifyWindow = new TripAddModifyWindow(tripGrid);
    TripManipulation tripManipulation = new TripManipulation(this, tripAddModifyWindow);
    PlaceManipulation placeManipulation = new PlaceManipulation(this, placeAddModifyWindow);

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        WindowUtil.setUiToShow(this);
        Session session = HibernateUtil.getSessionFactory().openSession();
        tripGrid.addObserver(tripManipulation);
        tripGrid.addObserver(placeGrid);
        tripGrid.addObserver(placeManipulation);
        tripGrid.addObserver(placeManipulation);

        tripManipulation.addObserver(tripGrid);
        tripManipulation.addObserver(placeGrid);

        placeGrid.addObserverPlace(placeManipulation);
        placeManipulation.addObserverPlace(placeGrid);

        verticalTrip.addComponent(tripGrid);
        verticalTrip.addComponent(tripManipulation);
        verticalPlace.addComponent(placeGrid);
        verticalPlace.addComponent(placeManipulation);


        horizontalLayout.addComponent(verticalTrip);
        horizontalLayout.addComponent(verticalPlace);
        setContent(horizontalLayout);
    }

}
