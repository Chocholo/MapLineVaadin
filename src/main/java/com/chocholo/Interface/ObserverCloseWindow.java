package com.chocholo.Interface;


import com.vaadin.ui.Window;

/**
 * Created by Chocholo on 19.02.2017.
 */
public interface ObserverCloseWindow {
    void update(Window window);
}
