package com.chocholo.Interface;

import com.chocholo.entity.Place;

/**
 * Created by Chocholo on 19.02.2017.
 */
public interface ObserverPlace {
    void update(Place place);
}
