package com.chocholo.Interface;

import org.hibernate.Session;

import java.util.List;

/**
 * Created by Chocholo on 16.02.2017.
 */
@FunctionalInterface
public interface InTransactionList<E> {
    List<E> withinTransactionList(Session session);
}
