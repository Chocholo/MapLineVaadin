package com.chocholo.Interface;

/**
 * Created by Chocholo on 19.02.2017.
 */
public interface ObservableCloseWindow {
    void addObserverCloseWindow(ObserverCloseWindow observerCloseWindow);
    void deleteObserverCloseWindow(ObserverCloseWindow observerCloseWindow);
    void notifyAllObserverCloseWindow();
}
