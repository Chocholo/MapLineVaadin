package com.chocholo.Interface;

import org.hibernate.Session;

/**
 * Created by Chocholo on 16.02.2017.
 */
@FunctionalInterface
public interface InTransactionVoid {
    void withinTransactionVoid(Session session);
}
