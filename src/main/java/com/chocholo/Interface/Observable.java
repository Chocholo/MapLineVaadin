package com.chocholo.Interface;

/**
 * Created by Chocholo on 15.02.2017.
 */
public interface Observable {
    void addObserver(Observer observer);
    void deleteObserver(Observer observer);
    void notifyObservers();
}
