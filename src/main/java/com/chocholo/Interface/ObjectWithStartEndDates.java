package com.chocholo.Interface;

import java.util.Date;

/**
 * Created by Chocholo on 16.02.2017.
 */
public interface ObjectWithStartEndDates {
    Date getStart();
    Date getEnd();
}
