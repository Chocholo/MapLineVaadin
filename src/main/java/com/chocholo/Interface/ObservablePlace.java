package com.chocholo.Interface;

/**
 * Created by Chocholo on 19.02.2017.
 */
public interface ObservablePlace  {
    void addObserverPlace(ObserverPlace observerPlace);
    void deleteObserverPlace(ObserverPlace observablePlace);
    void notifyAllObserverPlace();
}
