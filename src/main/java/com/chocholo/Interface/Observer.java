package com.chocholo.Interface;

import com.chocholo.entity.Trip;

/**
 * Created by Chocholo on 15.02.2017.
 */
public interface Observer {
    void update(Trip trip);
}
