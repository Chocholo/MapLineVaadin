package com.chocholo.Util;

import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Chocholo on 19.02.2017.
 */
public class WindowUtil {

    @Getter
    @Setter
    static UI uiToShow;

    private WindowUtil() {}
    public static void openWindow(Window window) {
        uiToShow.addWindow(window);
    }
}
