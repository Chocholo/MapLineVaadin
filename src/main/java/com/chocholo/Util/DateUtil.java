package com.chocholo.Util;

import com.chocholo.Interface.ObjectWithStartEndDates;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;

import java.text.DateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Created by Chocholo on 16.02.2017.
 */
public class DateUtil {

    private DateUtil() {}

    public static void setDateFormat(Grid grid, String columnName, int dateFormat) {
        Grid.Column column = grid.getColumn(columnName);
        column.setRenderer(new DateRenderer(DateFormat.getDateInstance(dateFormat)));
    }

    public static int compareDates(ObjectWithStartEndDates objectWithDates) {
        Optional<Date> dateStart = Optional.ofNullable(objectWithDates.getStart());
        Optional<Date> dateEnd = Optional.ofNullable(objectWithDates.getEnd());
        return dateStart.orElse(new Date(Long.MIN_VALUE)).compareTo(dateEnd.orElse(new Date(Long.MAX_VALUE)));
    }
}
