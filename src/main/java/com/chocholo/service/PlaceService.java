package com.chocholo.service;

import com.chocholo.Util.HibernateUtil;
import com.chocholo.entity.Place;
import com.chocholo.entity.Trip;
import com.google.gwt.i18n.client.Messages;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Chocholo on 15.02.2017.
 */
public class PlaceService {

    private PlaceService() {}

    public static PlaceService getInstance() {
        return new PlaceService();
    }

    public List<Place> getAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Place");
        List<Place> places =  query.getResultList();
        session.close();
        return places;
    }

    public void save(Place place) {
        ServiceUtils.wrapWithTransactionVoid(session -> {
            if(place.getId() < 1) { // new one
                place.setPositionInTrip(getNextPlaceInTrip(session, place));
            }
            session.saveOrUpdate(place);
        });
    }

    public void delete(Place place) {
        ServiceUtils.wrapWithTransactionVoid(session -> session.delete(place));
    }

    public List<Place> getVisitedPlacesFromTrip(Trip selectedTrip) {
        return ServiceUtils.wrapWithTransactionReturnList(session -> {
            System.out.println("WTF");
            Trip trip = null;
            if(selectedTrip != null) trip = session.find(Trip.class, selectedTrip.getId());
            List<Place> places = Optional.ofNullable(trip).map(Trip::getVisitedPlaces).orElse(new ArrayList<>());
            return places.stream().sorted(Comparator.comparingInt(Place::getPositionInTrip)).collect(Collectors.toList());
        });
    }

    private int getNextPlaceInTrip(Session session, Place place) {
        TypedQuery<Place> query = session.createQuery("from Place place where place.trip = :trip and place.positionInTrip = (select max(place2.positionInTrip) from Place place2)", Place.class);
        query.setParameter("trip", place.getTrip());
        Optional<Place> placeWithMax = Optional.ofNullable(query.getResultList().stream().findFirst().orElse(null));
        int positionInTrip = placeWithMax.map(place1 -> place1.getPositionInTrip()).orElse(-1);
        return positionInTrip+1;
    }

}
