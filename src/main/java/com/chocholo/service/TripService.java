package com.chocholo.service;

import com.chocholo.Util.HibernateUtil;
import com.chocholo.entity.Trip;
import lombok.NonNull;
import lombok.experimental.NonFinal;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.List;

/**
 * Created by Chocholo on 15.02.2017.
 */
public class TripService {

    static TripService tripService = null;
    public static TripService getInstance() {;
        if(tripService == null) {
            tripService = new TripService();
        }
        return tripService;
    }

    private TripService() {}

    public List<Trip> getAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Trip");
        List<Trip> trips = query.getResultList();
        session.close();
        return  trips;
    }

    public void save(@NonNull Trip trip) {
        ServiceUtils.wrapWithTransactionVoid(session -> session.saveOrUpdate(trip));
    }

    public void delete(Trip trip) {
        ServiceUtils.wrapWithTransactionVoid(session -> {
            session.remove(trip);
        });
    }

}
