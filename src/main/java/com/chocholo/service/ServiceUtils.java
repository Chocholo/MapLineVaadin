package com.chocholo.service;

import com.chocholo.Interface.InTransactionList;
import com.chocholo.Interface.InTransactionVoid;
import com.chocholo.Util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Chocholo on 16.02.2017.
 */
public class ServiceUtils {

    private ServiceUtils() {}

    static void wrapWithTransactionVoid(InTransactionVoid inTransaction) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        inTransaction.withinTransactionVoid(session);
        session.getTransaction().commit();
        session.close();
    }

    static <E> List<E> wrapWithTransactionReturnList(InTransactionList inTransaction) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        List<E> list = inTransaction.withinTransactionList(session);
        session.getTransaction().commit();
        session.close();
        return list;
    }

}
