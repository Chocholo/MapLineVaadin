package com.chocholo.entity;

import com.chocholo.Interface.ObjectWithStartEndDates;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by Chocholo on 15.02.2017.
 */
@Entity
@Data
public class Place implements ObjectWithStartEndDates {

    public Place() {}
    public Place(Trip trip) {
        this.trip = trip;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotNull
    Integer positionInTrip;

    @NotNull
    String name;

    @NotNull
    String descriptionShort;

    @NotNull
    @Column(length = 2000)
    String descriptionLong;

    @Embedded
    GeoPosition geoPosition;

    Date start;

    Date end;

    @ManyToOne
    Trip trip;
}
