package com.chocholo.entity;

import com.chocholo.Interface.ObjectWithStartEndDates;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Created by Chocholo on 15.02.2017.
 */
@Entity
@Data
public class Trip implements ObjectWithStartEndDates {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotNull
    @Size(min = 1)
    String name;

    @NotNull
    @Size(min = 1)
    String descriptionShort;

    @NotNull
    @Column(length = 2000)
    @Size(min = 1)
    String descriptionLong;

    Date start;

    Date end;

    Integer grade;

    @OneToMany(mappedBy = "trip", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Place> visitedPlaces;

}
