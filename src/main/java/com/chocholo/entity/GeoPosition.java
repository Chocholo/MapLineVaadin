package com.chocholo.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.persistence.Embeddable;

/**
 * Created by Chocholo on 15.02.2017.
 */
@Embeddable
@Data
public class GeoPosition {
    @SerializedName("lat")
    Float latitude;
    @SerializedName("lng")
    Float longitude;
}
