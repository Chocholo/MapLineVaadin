package com.chocholo.VaadinComponents.Place;

import com.chocholo.Interface.ObservableCloseWindow;
import com.chocholo.Interface.Observer;
import com.chocholo.Interface.ObserverCloseWindow;
import com.chocholo.entity.Trip;
import com.vaadin.ui.Window;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Chocholo on 16.02.2017.
 */
public class PlaceAddModifyWindow extends Window implements Observer, ObservableCloseWindow{

    PlaceForm placeForm = new PlaceForm(this);
    Set<ObserverCloseWindow> observers = new HashSet<>();
    public PlaceAddModifyWindow(Observer placeGrid) {
        setContent(placeForm);
        placeForm.addObserver(placeGrid);
        placeForm.addObserver(this);
        addCloseListener(closeEvent -> {
            notifyAllObserverCloseWindow();
        });
    }

    @Override
    public void update(Trip trip) {
        close();
    }

    @Override
    public void addObserverCloseWindow(ObserverCloseWindow observerCloseWindow) {
        observers.add(observerCloseWindow);
    }

    @Override
    public void deleteObserverCloseWindow(ObserverCloseWindow observerCloseWindow) { observers.remove(observerCloseWindow); }

    @Override
    public void notifyAllObserverCloseWindow() {
        observers.forEach(observer -> observer.update(this));
    }
}
