package com.chocholo.VaadinComponents.Place;

import com.chocholo.Interface.*;
import com.chocholo.Util.DateUtil;
import com.chocholo.entity.Place;
import com.chocholo.entity.Trip;
import com.chocholo.service.PlaceService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Chocholo on 15.02.2017.
 */

public class PlaceGrid extends Grid implements GridWithData, Observer, ObserverPlace, ObservablePlace {

    Trip selectedTrip;
    Place selectedPlace;
    PlaceService placeService = PlaceService.getInstance();
    Set<ObserverPlace> observers = new HashSet<>();

    public PlaceGrid() {
        setColumns("name");
        addSelectionListener(selectionEvent -> {
            selectedPlace = (Place) getSelectedRow();
            notifyAllObserverPlace();
        });
       // columnConfig();
    }

    @Override
    public void updateList() {
        List<Place> places = placeService.getVisitedPlacesFromTrip(selectedTrip);
        setContainerDataSource(new BeanItemContainer<>(Place.class, places));
    }

    public void columnConfig() {
        DateUtil.setDateFormat(this, "start", DateFormat.SHORT);
        DateUtil.setDateFormat(this, "end", DateFormat.SHORT);
    }

    @Override
    public void update(Trip trip) {
        selectedTrip = trip;
        updateList();
    }

    @Override
    public void update(Place place) { updateList(); }

    @Override
    public void addObserverPlace(ObserverPlace observerPlace) {
        observers.add(observerPlace);
    }

    @Override
    public void deleteObserverPlace(ObserverPlace observerPlace) {
        observers.remove(observerPlace);
    }

    @Override
    public void notifyAllObserverPlace() {
        observers.forEach(observer -> observer.update(selectedPlace));
    }
}
