package com.chocholo.VaadinComponents.Place;

import com.chocholo.Interface.*;
import com.chocholo.Util.DateUtil;
import com.chocholo.Util.WindowUtil;
import com.chocholo.VaadinComponents.GeoPosition.FindGeoPositionWindow;
import com.chocholo.entity.GeoPosition;
import com.chocholo.entity.Place;
import com.chocholo.service.PlaceService;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.ui.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Chocholo on 16.02.2017.
 */
public class PlaceForm extends FormLayout implements Observable {

    Set<Observer> observers = new HashSet<>();
    private ObservableCloseWindow window;
    private Place place;
    private GeoPosition geoPosition;
    private TextField name = new TextField("Place name");
    private TextField descriptionShort = new TextField("Short description");
    private TextArea descriptionLong = new TextArea("Long description");
    private DateField start = new DateField("Start date");
    private DateField end = new DateField("End date");
    private TextField latitude = new TextField("Latitude");
    private TextField longitude = new TextField("Longitude");
    private Button submit = new Button("submit");
    private Button findGeoPosition = new Button("find geoposition");
    private PropertysetItem item = new PropertysetItem();

    public PlaceForm(ObservableCloseWindow window) {
        nameConfig();
        descriptionShortConfig();
        descriptionLongConfig();
        startConfig();
        endConfig();
        findGeoPositionConfig();
        latitudeConfig();
        longitudeConfig();
        submitConfig();
        this.window = window;
    }

    void setPlace(Place place) {
        this.place = place;
        if(place.getGeoPosition() == null) {
            this.geoPosition = new GeoPosition();
            place.setGeoPosition(geoPosition);
        }
        bindFields();

    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(place.getTrip()));
    }

    private void findGeoPositionConfig() {
        findGeoPosition.addClickListener(e -> {
            FindGeoPositionWindow findGeoPositionWindow = new FindGeoPositionWindow(name.getValue());
            WindowUtil.openWindow(findGeoPositionWindow);
            window.addObserverCloseWindow(findGeoPositionWindow);
        });
        addComponent(findGeoPosition);
    }

    private void submitConfig() {
        submit.addClickListener(e -> {
            PlaceService.getInstance().save(place);
            notifyObservers();
            Notification.show("Success!", "Trip has been added to the database", Notification.Type.HUMANIZED_MESSAGE);
        });
        addComponent(submit);
    }

    private void longitudeConfig() {
        longitude.setMaxLength(255);
        longitude.setRequired(true);
        longitude.setNullRepresentation("");
        addComponent(longitude);
    }

    private void latitudeConfig() {
        latitude.setMaxLength(255);
        latitude.setRequired(true);
        latitude.setNullRepresentation("");
        addComponent(latitude);
    }

    private void endConfig() {
        end.addValueChangeListener((Property.ValueChangeListener) valueChangeEvent -> {
            if(DateUtil.compareDates(place) == 1) {
                Notification.show("Bad date!", "The end date is before the start date of your trip", Notification.Type.WARNING_MESSAGE);
                place.setEnd(null);
            }
        });
        addComponent(end);
    }

    private void startConfig() {
        start.addValueChangeListener((Property.ValueChangeListener) valueChangeEvent -> {
            if(DateUtil.compareDates(place) == 1) {
                place.setStart(null);
                Notification.show("Bad date!", "The start date is after the end date of your trip", Notification.Type.WARNING_MESSAGE);
            }
        });
        addComponent(start);
    }

    private void descriptionLongConfig() {
        descriptionLong.setRequired(true);
        descriptionLong.setMaxLength(2000);
        descriptionLong.setNullRepresentation("");
        addComponent(descriptionLong);
    }

    private void descriptionShortConfig() {
        descriptionShort.setMaxLength(255);
        descriptionShort.setRequired(true);
        descriptionShort.setNullRepresentation("");
        addComponent(descriptionShort);
    }

    private void nameConfig() {
        name.setRequired(true);
        name.setNullRepresentation("");
        name.setMaxLength(255);
        addComponent(name);
    }

    private void bindFields() {
        BeanFieldGroup beanFieldGroup = BeanFieldGroup.bindFieldsUnbuffered(place, this);
        beanFieldGroup.bind(longitude, "geoPosition.longitude");
        beanFieldGroup.bind(latitude, "geoPosition.latitude");
        beanFieldGroup.setItemDataSource(place);
    }

}
