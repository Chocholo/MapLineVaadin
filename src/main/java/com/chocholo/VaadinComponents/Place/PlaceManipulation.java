package com.chocholo.VaadinComponents.Place;

import com.chocholo.Interface.ObservablePlace;
import com.chocholo.Interface.Observer;
import com.chocholo.Interface.ObserverPlace;
import com.chocholo.VaadinComponents.Place.PlaceAddModifyWindow;
import com.chocholo.entity.Place;
import com.chocholo.entity.Trip;
import com.chocholo.service.PlaceService;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Chocholo on 16.02.2017.
 */
public class PlaceManipulation extends HorizontalLayout implements Observer, ObserverPlace, ObservablePlace {

    Trip chosenTrip;
    Place selectedPlace;
    Set<ObserverPlace> observersPlace = new HashSet<>();
    Button add = new Button("Add place");
    Button edit = new Button("Edit place");
    Button delete = new Button("delete place");
    public PlaceManipulation(UI uiToShowWindow, PlaceAddModifyWindow placeAddModifyWindow) {
        addConfig(uiToShowWindow, placeAddModifyWindow);
        editConfig(uiToShowWindow, placeAddModifyWindow);
        deleteConfig(uiToShowWindow, placeAddModifyWindow);
    }

    private void deleteConfig(UI uiToShowWindow, PlaceAddModifyWindow placeAddModifyWindow) {
        addComponent(delete);
        add.setEnabled(false);
        add.addClickListener(clickEvent -> {
            placeAddModifyWindow.placeForm.setPlace(new Place(chosenTrip));
            uiToShowWindow.addWindow(placeAddModifyWindow);
        });
    }

    private void editConfig(UI uiToShowWindow, PlaceAddModifyWindow placeAddModifyWindow) {
        addComponent(edit);
        edit.setEnabled(false);
        edit.addClickListener(clickEvent -> {
            placeAddModifyWindow.placeForm.setPlace(selectedPlace);
            uiToShowWindow.addWindow(placeAddModifyWindow);
        });
    }

    private void addConfig(UI uiToShowWindow, PlaceAddModifyWindow placeAddModifyWindow) {
        addComponent(add);
        delete.setEnabled(false);
        delete.addClickListener(clickEvent -> {
            PlaceService.getInstance().delete(selectedPlace);
            notifyAllObserverPlace();
        });
    }

    @Override
    public void update(Trip trip) {
        chosenTrip = trip;
        add.setEnabled(true);
    }

    @Override
    public void update(Place place) {
        edit.setEnabled(place != null);
        delete.setEnabled(place != null);
        selectedPlace = place;
    }

    @Override
    public void addObserverPlace(ObserverPlace observerPlace) {
        observersPlace.add(observerPlace);
    }

    @Override
    public void deleteObserverPlace(ObserverPlace observablePlace) {
        observersPlace.remove(observablePlace);
    }

    @Override
    public void notifyAllObserverPlace() {
        observersPlace.forEach(observerPlace -> observerPlace.update(selectedPlace));
    }
}
