package com.chocholo.VaadinComponents.Trip;

import com.chocholo.Interface.GridWithData;
import com.chocholo.Interface.Observable;
import com.chocholo.Interface.Observer;
import com.chocholo.Util.DateUtil;
import com.chocholo.entity.Trip;
import com.chocholo.service.TripService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Chocholo on 15.02.2017.
 */
public class TripGrid extends Grid implements Observer, Observable, GridWithData {

    private Trip selectedTrip;
    private TripService tripService = TripService.getInstance();
    private Set<Observer> observers = new HashSet<Observer>();

    public TripGrid() {
        setColumns("name", "start", "end", "grade");
        updateList();
        ColumnsConfig();
        addSelectionListener(selectionEvent -> {
           selectedTrip = (Trip)getSelectedRow();
           notifyObservers();
        });
    }

    @Override
    public void updateList() {
        List<Trip> trips = tripService.getAll();
        setContainerDataSource(new BeanItemContainer<>(Trip.class, trips));
    }

    @Override
    public void update(Trip trip) {
        updateList();
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(selectedTrip));
    }

    private void ColumnsConfig() {
        DateUtil.setDateFormat(this, "start", DateFormat.SHORT);
        DateUtil.setDateFormat(this, "end", DateFormat.SHORT);
    }

}
