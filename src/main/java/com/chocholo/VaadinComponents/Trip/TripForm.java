package com.chocholo.VaadinComponents.Trip;

import com.chocholo.Interface.Observable;
import com.chocholo.Interface.Observer;
import com.chocholo.Util.DateUtil;
import com.chocholo.entity.Trip;
import com.chocholo.service.TripService;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.filter.Not;
import com.vaadin.ui.*;

import java.lang.annotation.Native;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Chocholo on 15.02.2017.
 */
public class TripForm extends FormLayout implements Observable {

    private Trip trip;
    private Set<Observer> observers = new HashSet<>();
    private TextField name = new TextField("Trip name");
    private TextField descriptionShort = new TextField("Short description");
    private TextArea descriptionLong = new TextArea("Long description");
    private DateField start = new DateField("Trip's start");
    private DateField end = new DateField("Trip's end");
    private NativeSelect grade = new NativeSelect("Your grade");
    private Button submit = new Button("Submit");

    public TripForm() {
        nameConfig();
        descriptionShortConfig();
        descriptionLongConfig();
        startConfig();
        endConfig();
        gradeConfig();
        setTrip(new Trip());
        submitConfig();
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(trip));
    }

    void setTrip(Trip trip) {
        this.trip = trip;
        BeanFieldGroup.bindFieldsUnbuffered(trip, this);
    }

    private void nameConfig() {
        name.setRequired(true);
        name.setMaxLength(255);
        name.setNullRepresentation("");
        addComponent(name);
    }

    private void descriptionShortConfig() {
        descriptionShort.setRequired(true);
        descriptionShort.setMaxLength(255);
        descriptionShort.setNullRepresentation("");
        addComponent(descriptionShort);
    }

    private void descriptionLongConfig() {
        descriptionLong.setRequired(true);
        descriptionLong.setMaxLength(2000);
        descriptionLong.setNullRepresentation("");
        addComponent(descriptionLong);
    }

    private void startConfig() {
        start.addValueChangeListener((Property.ValueChangeListener) valueChangeEvent -> {
            if(DateUtil.compareDates(trip) == 1) {
                trip.setStart(null);
                Notification.show("Bad date!", "The start date is after the end date of your trip", Notification.Type.WARNING_MESSAGE);
            }
        });
        addComponent(start);
    }

    private void endConfig() {
        end.addValueChangeListener((Property.ValueChangeListener) valueChangeEvent -> {
            if(DateUtil.compareDates(trip) == 1) {
                Notification.show("Bad date!", "The end date is before the start date of your trip", Notification.Type.WARNING_MESSAGE);
                trip.setEnd(null);
            }
        });
        addComponent(end);
    }

    private void gradeConfig() {
        grade.setNullSelectionAllowed(true);
        List<Integer> gradeList = IntStream.rangeClosed(1, 10).boxed().collect(Collectors.toList());
        grade.addItems(gradeList);
        addComponent(grade);
    }

    private void submitConfig() {
        submit.addClickListener(e -> {
            TripService.getInstance().save(trip);
            notifyObservers();
            Notification.show("Success!", "Trip added to database", Notification.Type.HUMANIZED_MESSAGE);
        });
        addComponent(submit);
    }

}