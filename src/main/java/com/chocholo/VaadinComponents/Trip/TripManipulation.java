package com.chocholo.VaadinComponents.Trip;

import com.chocholo.Interface.Observable;
import com.chocholo.Interface.Observer;
import com.chocholo.entity.Trip;
import com.chocholo.service.TripService;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Chocholo on 15.02.2017.
 */
public class TripManipulation extends HorizontalLayout implements Observer, Observable {

    Set<Observer> observers = new HashSet<>();
    Trip selectedTrip;
    Button addTrip = new Button("Add Trip");
    Button editTrip = new Button("Edit selected Trip");
    Button deleteTrip = new Button("Delete selected Trip");

    public TripManipulation(UI uiToShowWindow, TripAddModifyWindow tripAddModifyWindow) {;
        setSpacing(true);
        addComponents(addTrip, editTrip, deleteTrip);
        addConfig(uiToShowWindow, tripAddModifyWindow);
        editConfig(uiToShowWindow, tripAddModifyWindow);
        deleteConfig(uiToShowWindow, tripAddModifyWindow);
    }

    private void deleteConfig(UI uiToShowWindow, TripAddModifyWindow tripAddModifyWindow) {
        deleteTrip.setEnabled(false);
        deleteTrip.addClickListener(clickEvent -> {
            TripService.getInstance().delete(selectedTrip);
            notifyObservers();
            Notification.show("Deleted!", "Successfully removed from database", Notification.Type.HUMANIZED_MESSAGE);
        });
    }

    private void editConfig(UI uiToShowWindow, TripAddModifyWindow tripAddModifyWindow) {
        editTrip.setEnabled(false);
        editTrip.addClickListener(clickEvent -> {
            tripAddModifyWindow.tripForm.setTrip(selectedTrip);
            uiToShowWindow.addWindow(tripAddModifyWindow);
        });
    }

    private void addConfig(UI uiToShowWindow, TripAddModifyWindow tripAddModifyWindow) {
        addTrip.addClickListener(clickEvent -> {
            tripAddModifyWindow.tripForm.setTrip(new Trip());
            uiToShowWindow.addWindow(tripAddModifyWindow);
        });
    }

    @Override
    public void update(Trip trip) {
        selectedTrip = trip;
        if(trip != null) {
            editTrip.setEnabled(true);
            deleteTrip.setEnabled(true);
        }
        else {
            editTrip.setEnabled(false);
            deleteTrip.setEnabled(false);
        }
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(null));
    }
}
