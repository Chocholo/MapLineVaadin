package com.chocholo.VaadinComponents.Trip;

import com.chocholo.Interface.Observer;
import com.chocholo.entity.Trip;
import com.vaadin.ui.Window;

/**
 * Created by Chocholo on 15.02.2017.
 */
public class TripAddModifyWindow extends Window implements Observer {

    TripForm tripForm = new TripForm();

    public TripAddModifyWindow(Observer tripGrid) {
        setContent(tripForm);
        tripForm.addObserver(tripGrid);
        tripForm.addObserver(this);
    }

    @Override
    public void update(Trip trip) {
        close();
    }
}
