package com.chocholo.VaadinComponents.GeoPosition;

import com.chocholo.GoogleApi.GooglePlaceApiRest;
import com.chocholo.entity.GeoPosition;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * Created by Chocholo on 19.02.2017.
 */
public class FindGeoPositionForm extends FormLayout {
    GeoPosition geoPosition = new GeoPosition();
    TextField placeName = new TextField("Place name");
    TextField latitude = new TextField("Latitude");
    TextField longitude = new TextField("Longitude");

    FindGeoPositionForm(String placeName) {
        placeNameConfig(placeName);
        geoPositionConfig();
    }

    private void placeNameConfig(String placeName) {
        this.placeName.setValue(placeName);
        this.placeName.setNullRepresentation("");
        this.placeName.setInputPrompt("Place to find");
        this.placeName.addValueChangeListener(value -> {
            findGeoAndSetValues();
        });
        addComponent(this.placeName);
    }

    private void geoPositionConfig() {
        latitude.setEnabled(false);
        longitude.setEnabled(false);
        latitude.setNullRepresentation("");
        longitude.setNullRepresentation("");
        addComponent(latitude);
        addComponent(longitude);
    }

    private void findGeoAndSetValues() {
        geoPosition = GooglePlaceApiRest.findPlace(this.placeName.getValue());
        latitude.setValue(geoPosition.getLatitude().toString());
        longitude.setValue(geoPosition.getLongitude().toString());
    }

}
