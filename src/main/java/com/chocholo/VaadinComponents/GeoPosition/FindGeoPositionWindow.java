package com.chocholo.VaadinComponents.GeoPosition;

import com.chocholo.Interface.ObserverCloseWindow;
import com.chocholo.VaadinComponents.GeoPosition.FindGeoPositionForm;
import com.vaadin.ui.Window;

/**
 * Created by Chocholo on 19.02.2017.
 */
public class FindGeoPositionWindow extends Window implements ObserverCloseWindow {
    private FindGeoPositionForm findGeoPositionForm;

    public  FindGeoPositionWindow(String placeName) {
        findGeoPositionForm = new FindGeoPositionForm(placeName);
        setContent(findGeoPositionForm);
    }

    @Override
    public void update(Window window) {
        close();
    }

}
