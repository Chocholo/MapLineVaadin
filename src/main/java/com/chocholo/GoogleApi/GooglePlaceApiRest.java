package com.chocholo.GoogleApi;

import com.chocholo.entity.GeoPosition;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chocholo on 19.02.2017.
 */
public class GooglePlaceApiRest {
    final static String key = "AIzaSyDwR1Oj6Z-pB-r04oWqq1X35rpl7OyVUUs";
    final static String link = "https://maps.googleapis.com/maps/api/place/textsearch/json";
    final static Map<String, String> getParams = new HashMap<>();

    public static GeoPosition findPlace(String place) {
        String result = getRestResponse(place);
        GeoPosition geoPosition = convertJsonToGeoPosition(result);
        return  geoPosition;
    }

    private static GeoPosition convertJsonToGeoPosition(String result) {
        GeoPosition geoPosition = new GeoPosition();
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONObject location = jsonObject.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
            Gson gson = new Gson();
            geoPosition = gson.fromJson(location.toString(), GeoPosition.class);
        }
        catch(JSONException jsonException) {
            geoPosition = new GeoPosition();
        }
        finally {
            return geoPosition;
        }
    }

    private static String getRestResponse(String place) {
        Client client = ClientBuilder.newClient();
        String result = client.target(link)
                .queryParam("key", key)
                .queryParam("query", place)
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        return result;
    }
}
